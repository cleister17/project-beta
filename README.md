# CarCar

Team:

* Conor - Sales Microservice
* Dillon - Service Microservice

## Design

## Service microservice

The Service microservice consists of 2 models (Appointment and Technician) as well as an AutmobileVO model drawn from the Automobile model in the Inventory microservice. The Appointment model has 7 properties: STATUS_CHOICES, date_time, reason, status, vin, customer, and technician (which uses a ForeignKey). The Technician model has 3 properties: employee_id, first_name, and last_name. The Service microservice will track existing/create new technicians and service appointments.

## Sales microservice

The Sales microservice consists of 3 models (Salesperson, Customer, Sale) as well as an AutomobileVO model drawn from the Automobile model in the Inventory microservice. Salesperson has 3 properties: first_name, last_name, and employee_id. Customer has 4 properties: first_name, last_name, address, and phone_number. Sale has 4 properties: salesperson, customer, automobile, and price, with the former 3 being foreign keys. The Sales microservice will track existing/create new salespeople, customers, and individual sales.
