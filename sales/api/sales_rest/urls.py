from django.urls import path
from .views import (
    api_salespeople_list,
    api_show_salesperson,
    api_customer_list,
    api_show_customer,
    api_sale_list,
    api_show_sale,
    api_automobile_list
)


urlpatterns = [
    path("salespeople/", api_salespeople_list, name="api_salespeople_list"),
    path("salespeople/<int:id>/", api_show_salesperson, name="api_show_salesperson"),
    path("customers/", api_customer_list, name="api_customer_list"),
    path("customers/<int:id>/", api_show_customer, name="api_show_customer"),
    path("sales/", api_sale_list, name="api_sale_list"),
    path("sales/<int:id>/", api_show_sale, name="api_show_sale"),
    path("automobilevo/", api_automobile_list, name="api_automobile_list")
]
