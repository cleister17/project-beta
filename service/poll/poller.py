import os
import sys
import time
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")


def poll(testing=False):
    while True:
        print('Service poller polling for data')
        try:
            response = requests.get(
                'http://project-beta-inventory-api-1:8000/api/automobiles')
            vins = [item['vin'] for item in response.json()['autos']]

            AutomobileVO.objects.all().delete()
            AutomobileVO.objects.bulk_create(
                [AutomobileVO(vin=vin) for vin in vins])

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)
        if testing:
            break


if __name__ == "__main__":
    import django
    django.setup()

    from service_rest.models import AutomobileVO
    poll()
