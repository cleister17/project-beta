from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from .models import Technician, Appointment
from .serializers import TechnicianSerializer, AppointmentSerializer


@api_view(['GET', 'POST'])
@permission_classes([AllowAny])
def technicians(request):
    if request.method == 'GET':
        technicians = Technician.objects.all()
        serializer = TechnicianSerializer(technicians, many=True)
        return Response({'technicians': serializer.data})

    if request.method == 'POST':
        serializer = TechnicianSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE'])
@permission_classes([AllowAny])
def technician(request, id):
    technician = get_object_or_404(Technician, id=id)

    if request.method == 'GET':
        serializer = TechnicianSerializer(technician)
        return Response(serializer.data)

    if request.method == 'DELETE':
        technician.delete()
        return Response(status=status.HTTP_200_OK)


@api_view(['GET', 'POST'])
@permission_classes([AllowAny])
def appointments(request):
    if request.method == 'GET':
        appointments = Appointment.objects.all()
        serializer = AppointmentSerializer(appointments, many=True)
        return JsonResponse({'appointments': serializer.data}, safe=False)

    if request.method == 'POST':
        technician_id = request.data.get('technician')
        try:
            technician = Technician.objects.get(id=technician_id)
        except Technician.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = AppointmentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(technician=technician)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE'])
@permission_classes([AllowAny])
def appointment(request, id):
    appointment = get_object_or_404(Appointment, pk=id)

    if request.method == 'GET':
        serializer = AppointmentSerializer(appointment)
        return Response(serializer.data)

    if request.method == 'DELETE':
        appointment.delete()
        return Response(status=status.HTTP_200_OK)


@api_view(['POST', 'PUT'])
@permission_classes([AllowAny])
def appointment_cancel(request, id):
    appointment = get_object_or_404(Appointment, pk=id)

    if request.method in ['POST', 'PUT']:
        appointment.status = 'canceled'
        appointment.save()
        serializer = AppointmentSerializer(appointment)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST', 'PUT'])
@permission_classes([AllowAny])
def appointment_finish(request, id):
    appointment = get_object_or_404(Appointment, pk=id)

    if request.method in ['POST', 'PUT']:
        appointment.status = 'finished'
        appointment.save()
        serializer = AppointmentSerializer(appointment)
        return Response(serializer.data, status=status.HTTP_200_OK)


def projects(request, *args, **kwargs):
    return appointments(request, *args, **kwargs)


def home(request):
    return HttpResponse("Welcome to the Home Page")
