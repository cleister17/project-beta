from rest_framework import serializers
from .models import Technician, Appointment

class TechnicianSerializer(serializers.ModelSerializer):
    class Meta:
        model = Technician
        fields = '__all__'

class AppointmentSerializer(serializers.ModelSerializer):
    technician = serializers.PrimaryKeyRelatedField(queryset=Technician.objects.all())

    class Meta:
        model = Appointment
        fields = '__all__'

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['technician'] = instance.technician.id
        return representation
