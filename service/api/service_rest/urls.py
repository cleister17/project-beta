from django.urls import path
from . import views

urlpatterns = [
    path('technicians/', views.technicians, name='technicians'),
    path('technicians/<int:id>/', views.technician, name='technician'),
    path('appointments/', views.appointments, name='appointments'),
    path('appointments/<int:id>/', views.appointment, name='appointment'),
    path('appointments/<int:id>/cancel/', views.appointment_cancel, name='appointment_cancel'),
    path('appointments/<int:id>/finish/', views.appointment_finish, name='appointment_finish'),
    path('projects/', views.appointments, name='projects'),
    path('appointments/', views.appointments, name='appointments'),
    path('', views.home, name='home'),
]
