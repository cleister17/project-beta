from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    employee_id = models.CharField(max_length=10)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)

    def __str__(self):
        return self.vin


class Appointment(models.Model):
    STATUS_CHOICES = (
        ('created', 'Created'),
        ('canceled', 'Canceled'),
        ('finished', 'Finished'),
    )
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=255)
    status = models.CharField(
        max_length=10, choices=STATUS_CHOICES, default='created')
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=255)
    technician = models.ForeignKey(Technician, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.customer} - {self.date_time} - {self.vin} - {self.status}"
