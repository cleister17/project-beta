import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Nav from './Nav';
import MainPage from './MainPage';
import SalespersonList from './Sales/SalespersonList';
import SalespersonForm from './Sales/SalespersonForm';
import CustomerList from './Sales/CustomerList';
import CustomerForm from './Sales/CustomerForm';
import SaleList from './Sales/SaleList';
import SaleForm from './Sales/SaleForm';
import SalespersonHistory from './Sales/SalespersonHistory';
import ManufacturerList from './Inventory/ManufacturerList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ModelList from './Inventory/ModelList';
import ModelForm from './Inventory/ModelForm';
import AutomobileList from './Inventory/AutomobileList';
import AutomobileForm from './Inventory/AutomobileForm';
import AddTechnician from './Service/AddTechnician';
import Technicians from './Service/Technicians';
import CreateAppointment from './Service/CreateAppointment';
import ServiceAppointments from './Service/ServiceAppointments';
import ServiceHistory from './Service/ServiceHistory';


function App() {
    return (
        <BrowserRouter>
        <Nav />
        <div className="container">
        <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="manufacturers/">
                <Route path="" element={<ManufacturerList />} />
                <Route path="form" element={<ManufacturerForm />} />
            </Route>
            <Route path="models/">
                <Route path="" element={<ModelList />} />
                <Route path="form" element={<ModelForm />} />
            </Route>
            <Route path="automobiles/">
                <Route path="" element={<AutomobileList />} />
                <Route path="form" element={<AutomobileForm />} />
            </Route>
            <Route path="salespeople/">
                <Route path="" element={<SalespersonList />} />
                <Route path="form" element={<SalespersonForm />} />
                <Route path="history" element={<SalespersonHistory />} />
            </Route>
            <Route path="customers/">
                <Route path="" element={<CustomerList />} />
                <Route path="form" element={<CustomerForm />} />
            </Route>
            <Route path="sales/">
                <Route path="" element={<SaleList />} />
                <Route path="form" element={<SaleForm />} />
            </Route>
            <Route path="/technicians" element={<Technicians />} />
            <Route path="/add-technician" element={<AddTechnician />} />
            <Route path="/create-appointment" element={<CreateAppointment />} />
            <Route path="/service-appointments" element={<ServiceAppointments />} />
            <Route path="/service-history" element={<ServiceHistory />} />
        </Routes>
        </div>
        </BrowserRouter>
    );
};


export default App;
