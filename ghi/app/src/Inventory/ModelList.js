import React, { useEffect, useState } from 'react';


function ModelList() {
    const [models, setModel] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModel(data.models);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div>
        <h1>Models</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
            </tr>
            </thead>
            <tbody>
            {models.map((model) => {
            return (
                <tr key={model.id} value={model.id}>
                    <td> {model.name} </td>
                    <td> {model.manufacturer.name} </td>
                    <td>
                        <img alt="" src={model.picture_url} width="100px" height="100px"/>
                    </td>
                </tr>
            );
        })}
            </tbody>
        </table>
        </div>
    );
};

export default ModelList;
