import React, { useState } from "react";


function ManufacturerForm() {
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;

        const url = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {

            setName('');
        }
    }
    return (
        <div className="my-5 container">
            <div className="row">
            <div className="col">
                <div className="card shadow">
                <div className="card-body">
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                    <h1 className="card-title">Add a manufacturer</h1>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input
                            onChange={handleNameChange}
                            required
                            placeholder="Name"
                            type="text"
                            id="name"
                            name="name"
                            className="form-control"
                            value={name}
                            />
                        <label htmlFor="name">Name</label>
                        </div>
                        <button className="btn btn-lg btn-primary">Create</button>
                    </div>
                    </form>
                </div>
                </div>
            </div>
            </div>
        </div>
        );
};

export default ManufacturerForm;
