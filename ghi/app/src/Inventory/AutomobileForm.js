import React, { useEffect, useState } from "react";


function AutomobileForm() {
    const [models, setModels] = useState([]);

    const [model, setModel] = useState('');
    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }
    const [vin, setVIN] = useState('');
    const handleVINChange = (event) => {
        const value = event.target.value;
        setVIN(value);
    }
    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const [year, setYear] = useState('');
    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.model_id = model;
        data.vin = vin;
        data.color = color;
        data.year = year;

        const automobilesURL = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response =  await fetch(automobilesURL, fetchConfig);
        if (response.ok) {

            setModel('');
            setVIN('');
            setColor('');
            setYear('');
        }
    }

    const fetchModels = async () => {
        const modelsURL = 'http://localhost:8100/api/models/';
        const response = await fetch(modelsURL);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(() => {
        fetchModels();
    }, []);

    return (
        <div className="my-5 container">
        <div className="row">
            <div className="col">
            <div className="card shadow">
                <div className="card-body">
                <form onSubmit={handleSubmit}>
                    <h1 className="card-title">Add an automobile to the inventory</h1>
                    <div className="form-floating mb-3">
                    <select
                        id="model"
                        name="model"
                        value={model}
                        onChange={handleModelChange}
                        className="form-select"
                        required
                    >
                        <option value="">Choose a model...</option>
                        {models.map((model) => (
                        <option key={model.id} value={model.id}>
                            {model.name}
                        </option>
                        ))}
                    </select>
                    <label htmlFor="model">Model</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input
                        id="vin"
                        type="text"
                        value={vin}
                        onChange={handleVINChange}
                        className="form-control"
                        required
                    />
                    <label htmlFor="vin">VIN...</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input
                        id="color"
                        type="text"
                        value={color}
                        onChange={handleColorChange}
                        className="form-control"
                        required
                    />
                    <label htmlFor="color">Color...</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input
                        id="year"
                        type="number"
                        value={year}
                        onChange={handleYearChange}
                        className="form-control"
                        required
                    />
                    <label htmlFor="year">Year...</label>
                    </div>
                    <button type="submit" className="btn btn-lg btn-primary">Add Automobile</button>
                </form>
                </div>
            </div>
            </div>
        </div>
        </div>
    );
};

export default AutomobileForm;
