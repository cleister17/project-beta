import React, { useState, useEffect } from "react";


function ModelForm() {
    const [manufacturers, setManufacturers] = useState([]);

    const [modelName, setModelName] = useState('');
    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }
    const [pictureURL, setPictureURL] = useState('');
    const handlePictureURLChange = (event) => {
        const value = event.target.value;
        setPictureURL(value);
    }
    const [manufacturer, setManufacturer] = useState('');
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = modelName;
        data.picture_url = pictureURL;
        data.manufacturer_id = manufacturer;

        const modelsURL = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(modelsURL, fetchConfig);
        if (response.ok) {

            setModelName('');
            setPictureURL('');
            setManufacturer('');
        }
    }
    const fetchManufacturers = async () => {
        const manufacturersURL = 'http://localhost:8100/api/manufacturers/';
        const response =  await fetch(manufacturersURL);
        if (response.ok) {
            const manufacturersData = await response.json();
            setManufacturers(manufacturersData.manufacturers);
        }
    }
    useEffect(() => {
        fetchManufacturers();
    }, []);

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit}>
                                <h1 className="card-title">Create a vehicle model</h1>
                                <div className="form-floating mb-3">
                                    <input
                                        type="text"
                                        id="modelName"
                                        name="modelName"
                                        className="form-control"
                                        placeholder=" "
                                        value={modelName}
                                        onChange={handleModelNameChange}
                                        required
                                    />
                                    <label htmlFor="modelName">Model name...</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input
                                        type="url"
                                        id="pictureURL"
                                        name="pictureURL"
                                        className="form-control"
                                        placeholder=" "
                                        value={pictureURL}
                                        onChange={handlePictureURLChange}
                                    />
                                    <label htmlFor="pictureURL">Picture URL...</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <select
                                        id="manufacturer"
                                        name="manufacturer"
                                        className="form-control"
                                        value={manufacturer}
                                        onChange={handleManufacturerChange}
                                    >
                                        <option value="">Choose a manufacturer...</option>
                                        {manufacturers.map((manufacturer) => (
                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        ))}
                                    </select>
                                    <label htmlFor="manufacturer">Manufacturer</label>
                                </div>
                                <button className="btn btn-lg btn-primary">Add Model</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ModelForm;
