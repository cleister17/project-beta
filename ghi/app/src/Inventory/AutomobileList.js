import React, { useEffect, useState } from 'react';


function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
        <h1>Automobiles</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Sold</th>
            </tr>
            </thead>
            <tbody>
            {automobiles.map((automobile) => {
            return (
                <tr key={automobile.id} value={automobile.id}>
                    <td key={automobile.vin}>{automobile.vin}</td>
                    <td key={automobile.color}>{automobile.color}</td>
                    <td key={automobile.year}>{automobile.year}</td>
                    <td key={automobile.model.name}>{automobile.model.name}</td>
                    <td key={automobile.model.manufacturer.name}>{automobile.model.manufacturer.name}</td>
                    <td key={automobile.sold}>{automobile.sold}</td>
                </tr>
            );
        })}
            </tbody>
        </table>
        </div>
    );
};

export default AutomobileList;
