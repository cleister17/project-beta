import React, { useEffect, useState } from 'react'


function ManufacturerList() {
    const [manufacturers, setManufacturer] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturer(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div>
        <h1>Manufacturers</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Manufacturers</th>
            </tr>
            </thead>
            <tbody>
            {manufacturers.map((manufacturer) => {
            return (
                <tr key={manufacturer.id} value={manufacturer.id}>
                    <td key={manufacturer.name}>{manufacturer.name}</td>
                </tr>
            );
        })}
            </tbody>
        </table>
        </div>
    )
}

export default ManufacturerList;
