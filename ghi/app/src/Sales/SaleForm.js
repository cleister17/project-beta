import React, { useEffect, useState } from "react";


function SaleForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [salespeople, setSalespeople] = useState([]);

    const [automobile, setAutomobile] = useState('');
    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }
    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const [salesperson, setSalesperson] = useState('');
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }
    const [price, setPrice] = useState('');
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.automobile = automobile;
        data.customer = customer;
        data.salesperson = salesperson;
        data.price = price;

        const salesURL = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response =  await fetch(salesURL, fetchConfig);
        if (response.ok) {

            setAutomobile('');
            setCustomer('');
            setSalesperson('');
            setPrice('');
        }
    }
    const fetchAutomobiles = async () => {
        const url = 'http://localhost:8090/api/automobilevo/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.automobiles);
        }
    }
    const fetchCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }
    const fetchSalespeople = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }
    useEffect(() => {
        fetchAutomobiles();
        fetchCustomers();
        fetchSalespeople();
    }, []);

    return (
    <div className="my-5 container">
        <div className="row">
        <div className="col">
            <div className="card shadow">
            <div className="card-body">
                <form onSubmit={handleSubmit}>
                <h1 className="card-title">Record a new sale</h1>
                <div className="form-floating mb-3">
                    <select
                    id="automobile"
                    name="automobile"
                    value={automobile}
                    onChange={handleAutomobileChange}
                    className="form-select"
                    required
                    >
                    <option value="">Choose an automobile VIN...</option>
                    {automobiles.map((automobile) => (
                        <option key={automobile.vin} value={automobile.vin}>
                        {automobile.vin}
                        </option>
                    ))}
                    </select>
                    <label htmlFor="automobile">Automobile VIN</label>
                </div>
                <div className="form-floating mb-3">
                    <select
                    id="customer"
                    name="customer"
                    value={customer}
                    onChange={handleCustomerChange}
                    className="form-select"
                    required
                    >
                    <option value="">Choose a customer...</option>
                    {customers.map((customer) => (
                        <option key={customer.id} value={customer.id}>
                        {customer.first_name} {customer.last_name}
                        </option>
                    ))}
                    </select>
                    <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                    <select
                    id="salesperson"
                    name="salesperson"
                    value={salesperson}
                    onChange={handleSalespersonChange}
                    className="form-select"
                    required
                    >
                    <option value="">Choose a salesperson...</option>
                    {salespeople.map((salesperson) => (
                        <option key={salesperson.employee_id} value={salesperson.id}>
                        {salesperson.first_name} {salesperson.last_name}
                        </option>
                    ))}
                    </select>
                    <label htmlFor="salesperson">Salesperson</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                    id="price"
                    type="number"
                    value={price}
                    onChange={handlePriceChange}
                    className="form-control"
                    required
                    />
                    <label htmlFor="price">Price</label>
                </div>
                <button type="submit" className="btn btn-lg btn-primary">Add Sale</button>
                </form>
            </div>
            </div>
        </div>
        </div>
    </div>
    )
};

export default SaleForm;
