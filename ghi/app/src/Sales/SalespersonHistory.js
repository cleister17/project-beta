import React, { useState, useEffect } from "react";


function SalespersonHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [sales, setSales] = useState([]);
    const [salesperson, setSalesperson] = useState(0);

    const handleSalespersonChange = (event) => {
        const value = parseInt(event.target.value);
        setSalesperson(value);
    }

    const fetchData = async () => {
        const salespeopleURL = "http://localhost:8090/api/salespeople/";
        const salespeopleResponse = await fetch(salespeopleURL);
        if (salespeopleResponse.ok) {
            const salespeopleData = await salespeopleResponse.json();
            setSalespeople(salespeopleData.salespeople);
        }

        const salesURL = "http://localhost:8090/api/sales/"
        const salesResponse = await fetch(salesURL);
        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            setSales(salesData.sales);
        }
    }

    const filteredSales = () => {
        const filtered = sales.filter(sale => sale.salesperson.id === salesperson);
        return filtered;
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
            <h1>Salesperson History</h1>
            <label htmlFor="salesperson-select">Select a Salesperson:</label>
            <select id="salesperson-select" value={salesperson} onChange={handleSalespersonChange}>
                <option value="">Select a Salesperson</option>
                {salespeople.map((salesperson) => (
                    <option key={salesperson.id} value={salesperson.id}>
                        {salesperson.first_name} {salesperson.last_name}
                    </option>
                ))}
            </select>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSales().map((sale) => {
                        return (
                        <tr key={sale.id} value={sale.id}>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                        );
                        })}
                </tbody>
            </table>
        </div>
    );
}

export default SalespersonHistory;
