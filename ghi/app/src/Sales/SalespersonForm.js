import React, { useState } from "react";


function SalespersonForm() {
    const [firstName, setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }
    const [lastName, setLastName] = useState('');
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const [employeeID, setEmployeeID] = useState('');
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.employee_id = employeeID;
        data.first_name = firstName;
        data.last_name = lastName;

        const url = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {

            setFirstName('');
            setLastName('');
            setEmployeeID('');
        }
    }
    return (
        <div className="my-5 container">
            <div className="row">
            <div className="col">
                <div className="card shadow">
                <div className="card-body">
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                    <h1 className="card-title">Create a salesperson</h1>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input
                            onChange={handleFirstNameChange}
                            required
                            placeholder="First Name"
                            type="text"
                            id="firstName"
                            name="firstName"
                            className="form-control"
                            value={firstName}
                            />
                        <label htmlFor="name">First Name</label>
                        </div>
                        <div className="col">
                        <div className="form-floating mb-3">
                            <input
                            onChange={handleLastNameChange}
                            required
                            placeholder="Last Name"
                            type="text"
                            id="lastName"
                            name="lastName"
                            className="form-control"
                            value={lastName}
                            />
                            <label htmlFor="name">Last Name</label>
                        </div>
                        </div>
                        <div className="col">
                        <div className="form-floating mb-3">
                            <input
                            onChange={handleEmployeeIDChange}
                            required
                            placeholder="Employee ID"
                            type="number"
                            id="employee_id"
                            name="employee_id"
                            className="form-control"
                            value={employeeID}
                            />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        </div>
                        <button className="btn btn-lg btn-primary">Create</button>
                    </div>
                    </form>
                </div>
                </div>
            </div>
            </div>
        </div>
        );
    };

export default SalespersonForm;
