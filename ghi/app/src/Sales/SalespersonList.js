import React, { useEffect, useState } from 'react'


function SalespersonList() {
    const [salespeople, setSalesperson] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salespeople)
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div>
        <h1>Salespeople</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Employee ID</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
            </thead>
            <tbody>
            {salespeople.map((salesperson) => {
            return (
                <tr key={salesperson.id} value={salesperson.id}>
                    <td key={salesperson.employee_id}>{salesperson.employee_id}</td>
                    <td key={salesperson.first_name}>{salesperson.first_name}</td>
                    <td key={salesperson.last_name}>{salesperson.last_name}</td>
                </tr>
            );
        })}
            </tbody>
        </table>
        </div>
    )
}

export default SalespersonList;
