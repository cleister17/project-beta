import React, { useState } from "react";


function CustomerForm() {
    const [firstName, setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }
    const [lastName, setLastName] = useState('');
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const [address, setAddress] = useState('');
    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }
    const [phoneNumber, setPhoneNumber] = useState('');
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

        const url = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {

            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
        }
    }
    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-customer-form">
                                <h1 className="card-title">Create a customer</h1>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                        onChange={handleFirstNameChange}
                                        required
                                        placeholder="First Name"
                                        type="text" id="firstName"
                                        name="firstName"
                                        className="form-control"
                                        value={firstName}
                                        />
                                        <label htmlFor="name">First Name</label>
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input
                                        onChange={handleLastNameChange}
                                        required
                                        placeholder="Last Name"
                                        type="text"
                                        id="lastName"
                                        name="lastName"
                                        className="form-control"
                                        value={lastName}
                                        />
                                        <label htmlFor="name">Last Name</label>
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input
                                        onChange={handleAddressChange}
                                        required
                                        placeholder="Address"
                                        type="text" id="address"
                                        name="address"
                                        className="form-control"
                                        value={address}
                                        />
                                        <label htmlFor="address">Address</label>
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input
                                        onChange={handlePhoneNumberChange}
                                        required
                                        placeholder="Phone Number"
                                        type="tel" id="phoneNumber"
                                        name="phoneNumber"
                                        className="form-control"
                                        value={phoneNumber}
                                        />
                                        <label htmlFor="phoneNumber">Phone Number</label>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default CustomerForm;
