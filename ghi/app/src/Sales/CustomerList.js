import React, { useEffect, useState } from 'react'


function CustomerList() {
    const [customers, setCustomer] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/customers';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomer(data.customers)
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div>
        <h1>Customers</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Address</th>
                <th>Phone Number</th>
            </tr>
            </thead>
            <tbody>
            {customers.map((customer) => {
            return (
                <tr key={customer.id} value={customer.id}>
                    <td key={customer.first_name}>{customer.first_name}</td>
                    <td key={customer.last_name}>{customer.last_name}</td>
                    <td key={customer.address}>{customer.address}</td>
                    <td key={customer.phone_number}>{customer.phone_number}</td>
                </tr>
            );
        })}
            </tbody>
        </table>
        </div>
    )
}

export default CustomerList;
