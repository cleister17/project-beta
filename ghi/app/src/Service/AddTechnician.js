import React, { useState } from 'react';
import './AddTechnician.css';

const AddTechnician = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeId, setEmployeeId] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();


    const url = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        first_name: firstName,
        last_name: lastName,
        employee_id: employeeId,
      }),
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        alert('Technician created successfully!');
        setFirstName('');
        setLastName('');
        setEmployeeId('');
      } else {
        alert('Failed to create technician.');
      }
    } catch (error) {
      console.error(error);
      alert('Failed to create technician.');
    }
  };

  return (
    <div className="add-technician-container">
      <h2>Add a Technician</h2>
      <form onSubmit={handleSubmit}>
        <label htmlFor="firstName">First Name:</label>
        <input
          type="text"
          id="firstName"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
        <br />
        <label htmlFor="lastName">Last Name:</label>
        <input
          type="text"
          id="lastName"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
        <br />
        <label htmlFor="employeeId">Employee ID:</label>
        <input
          type="text"
          id="employeeId"
          value={employeeId}
          onChange={(e) => setEmployeeId(e.target.value)}
          required
        />
        <br />
        <button type="submit" className="create-button">
          Create
        </button>
      </form>
    </div>
  );
};

export default AddTechnician;
