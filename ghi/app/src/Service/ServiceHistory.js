import { useState, useEffect } from 'react';
import './ServiceHistory.css';

function ServiceHistory() {
  const [searchVin, setSearchVin] = useState('');
  const [appointments, setAppointments] = useState([]);
  const [filteredAppointments, setFilteredAppointments] = useState([]);

  useEffect(() => {
    async function getAppointments() {
      try {
        const response = await fetch('http://localhost:8080/api/appointments/');
        const contentType = response.headers.get('content-type');
        if (contentType && contentType.indexOf('application/json') !== -1) {
          const data = await response.json();
          setAppointments(data.appointments);
          setFilteredAppointments(data.appointments);
        } else {
          console.error('Response is not in JSON format');
          throw new Error('Response is not in JSON format');
        }
      } catch (error) {
        console.error(error);
        throw error;
      }
    }

    getAppointments();
  }, []);

  const searchAppointments = () => {
    setFilteredAppointments(
      appointments.filter((appointment) => appointment.vin.includes(searchVin))
    );
  };

  const handleSearchChange = (e) => {
    setSearchVin(e.target.value);
  };

  return (
    <div className="service-history">
      <h2>Service History</h2>
      <div className="search-bar">
        <input
          type="text"
          placeholder="Search by VIN"
          value={searchVin}
          onChange={handleSearchChange}
        />
        <button onClick={searchAppointments}>Search</button>
      </div>
      <ul>
        <li className="appointment">
          <div className="appointment-details">
            <span className="vin"><b>VIN</b></span>
            <span className="customer-name"><b>Customer Name</b></span>
            <span className="appointment-date-time"><b>Appointment Date Time</b></span>
            <span className="technician"><b>Technician</b></span>
            <span className="reason"><b>Reason</b></span>
          </div>
          <div className="appointment-status">
            <span><b>Status</b></span>
          </div>
        </li>
        {filteredAppointments.map((appointment) => ( // Change this line
          <li key={appointment.id} className="appointment">
            <div className="appointment-details">
              <span className="vin">{appointment.vin}</span>
              {appointment.dealershipPurchased && <span className="vip">VIP</span>}
              <span className="customer-name">{appointment.customer}</span>
              <span className="appointment-date-time">
                {new Date(appointment.date_time).toLocaleString()}
              </span>
              <span className="technician">{appointment.technician}</span>
              <span className="reason">{appointment.reason}</span>
            </div>
            <div className="appointment-status">
              <span>{appointment.status}</span>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default ServiceHistory;
