import { useState, useEffect } from 'react';
import { fetchAppointments, deleteAppointment, updateAppointment } from './api';
import './ServiceAppointments.css';

export default function ServiceAppointments() {
  const [appointments, setAppointments] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const fetchedAppointments = await fetchAppointments();
      setAppointments(fetchedAppointments.appointments);
    }

    fetchData();
  }, []);

  const cancelAppointment = async (id) => {
    try {
      const updatedAppointment = await updateAppointment(id, { status: 'canceled' }, 'cancel');
      setAppointments(appointments.filter(appointment => appointment.id !== updatedAppointment.id));
    } catch (error) {
      console.error('Failed to update appointment status to canceled', error);
    }
  };

  const finishAppointment = async (id) => {
    try {
      const updatedAppointment = await updateAppointment(id, { status: 'finished' }, 'finish');
      setAppointments(appointments.filter(appointment => appointment.id !== updatedAppointment.id));
    } catch (error) {
      console.error('Failed to update appointment status to finished', error);
    }
  };

  return (
    <div className="service-appointments">
      <h2>Service Appointments</h2>
      <ul>
        {appointments.map((appointment) => (
          <li key={appointment.id} className="appointment">
            <div className="appointment-details">
              <span className="vin">{appointment.vin}</span>
              <span className="customer-name">{appointment.customerName}</span>
              <span className="appointment-date-time">
                {new Date(appointment.date_time).toLocaleString()}
              </span>
              <span className="technician">{appointment.technician.first_name} {appointment.technician.last_name}</span>
              <span className="reason">{appointment.reason}</span>
              {appointment.dealershipPurchased && <span className="vip">VIP</span>}
            </div>
            <div className="appointment-actions">
              <button onClick={() => cancelAppointment(appointment.id)}>Cancel</button>
              <button onClick={() => finishAppointment(appointment.id)}>Finish</button>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}
