import React, { useState, useEffect } from 'react';
import { fetchTechnicians, fetchCustomers, fetchVehicles } from './api';
import './CreateAppointment.css';


function CreateAppointment() {
  const [technicians, setTechnicians] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [vehicles, setVehicles] = useState([]);
  const [formData, setFormData] = useState({
    vin: '',
    customerName: '',
    appointmentDateTime: '',
    technician: '',
    reason: '',
  });

  useEffect(() => {
    async function fetchData() {
      try {
        const fetchedTechnicians = await fetchTechnicians();
        setTechnicians(fetchedTechnicians);

        const fetchedCustomers = await fetchCustomers();
        setCustomers(fetchedCustomers);

        const fetchedVehicles = await fetchVehicles();
        setVehicles(fetchedVehicles);
      } catch (err) {
        console.error('Error fetching data:', err);
      }
    }

    fetchData();
  }, []);

  const handleChange = (e) => {
    const value = e.target.type === 'select-one' ? e.target.selectedOptions[0].value : e.target.value;
    setFormData({ ...formData, [e.target.name]: value });
  };


  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const technicianId = Number(formData.technician);

      const response = await fetch('http://localhost:8080/api/appointments/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          date_time: formData.appointmentDateTime,
          reason: formData.reason,
          vin: formData.vin,
          customer: formData.customerName,
          technician: technicianId,
        }),
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(`Failed to create appointment: ${JSON.stringify(errorData)}`);
      }

      const appointmentData = await response.json();
      alert('Appointment created successfully');
    } catch (err) {
      console.error('Error creating appointment:', err);
    }
  };

  return (
    <div className="create-appointment-container">
      <h2>Create a Service Appointment</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="vin">Vehicle Identification Number (VIN):</label>
          <select
            id="vin"
            name="vin"
            value={formData.vin}
            onChange={handleChange}
            required>
            <option value="">Select a vehicle</option>
            {vehicles && vehicles.map((vehicle, index) => (
              <option key={vehicle.vin || index} value={vehicle.vin}>
                {vehicle.vin}
              </option>
            ))}
          </select>
        </div>
        <div>
          <label htmlFor="customerName">Customer Name:</label>
          <select
            id="customerName"
            name="customerName"
            value={formData.customerName}
            onChange={handleChange}
            required>
            <option value="">Select a customer</option>
            {customers && customers.map((customer, index) => (
              <option key={customer.id || index} value={customer.id}>
                {customer.first_name} {customer.last_name}
              </option>
            ))}
          </select>
        </div>
        <div>
          <label htmlFor="appointmentDateTime">Date and Time:</label>
          <input
            type="datetime-local"
            id="appointmentDateTime"
            name="appointmentDateTime"
            value={formData.appointmentDateTime}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <label htmlFor="technician">Assigned Technician:</label>
          <select
            id="technician"
            name="technician"
            value={formData.technician}
            onChange={handleChange}
            required
          >
            <option value="">Select a technician</option>
            {technicians && technicians.map((technician, index) => (
              <option key={technician.id || index} value={technician.id}>
                {technician.first_name} {technician.last_name}
              </option>
            ))}
          </select>
        </div>
        <div>
          <label htmlFor="reason">Reason for Service:</label>
          <input
            type="text"
            id="reason"
            name="reason"
            value={formData.reason}
            onChange={handleChange}
            required
          />
        </div>
        <button type="submit">Create Appointment</button>
      </form>
    </div>
  );
}

export default CreateAppointment;
