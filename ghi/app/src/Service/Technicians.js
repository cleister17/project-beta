import React, { useState, useEffect } from 'react';

const Technicians = () => {
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    fetch('http://localhost:8080/api/technicians/')
      .then((response) => response.json())
      .then((data) => {
        setTechnicians(data.technicians);
      })
      .catch((error) => {
        console.error('Error fetching technicians:', error);
      });
  }, []);

  return (
    <div>
      <h2>Technicians</h2>
      <table>
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {technicians.length > 0 &&
            technicians.map((technician, index) => (
              <tr key={technician.id}>
                <td>{technician.employee_id}</td>
                <td>{technician.first_name} {technician.last_name}</td>
              </tr>
            ))}
          {technicians.length === 0 && (
            <tr>
              <td colSpan="2">No technicians found.</td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default Technicians;
