const API_BASE_URL = 'http://localhost:8080';

export async function fetchTechnicians() {
  const response = await fetch(API_BASE_URL + '/api/technicians/');
  const data = await response.json();
  return data.technicians;
}

export async function fetchCustomers() {
  const response = await fetch('http://localhost:8090/api/customers/');
  const data = await response.json();
  return data.customers;
}

export async function fetchVehicles() {
  const response = await fetch('http://localhost:8090/api/automobilevo');
  const data = await response.json();
  return data.automobiles;
}

export async function fetchAppointments() {
  const response = await fetch(API_BASE_URL + '/api/appointments/');
  const data = await response.json();
  const activeAppointments = data.appointments.filter(
    (appointment) => appointment.status !== 'canceled' && appointment.status !== 'finished'
  );
  return { ...data, appointments: activeAppointments };
}

export async function deleteAppointment(id) {
  try {
    const response = await fetch(`${API_BASE_URL}/api/appointments/${id}/`, {
      method: 'DELETE',
    });

    if (!response.ok) {
      throw new Error('Failed to delete appointment');
    }
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export async function updateAppointment(id, data, action) {
  try {
    const response = await fetch(`${API_BASE_URL}/api/appointments/${id}/${action}/`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      throw new Error('Failed to update appointment');
    }

    return await response.json();
  } catch (error) {
    console.error('Error in updateAppointment:', error);
    throw error;
  }
}
