import { NavLink } from 'react-router-dom';

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-success">
            <div className="container-fluid">
                <NavLink className="navbar-brand" to="/">CarCar</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Manufacturers
                            </a>
                            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li>
                                    <NavLink className="dropdown-item" to="/manufacturers">View Manufacturers</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/manufacturers/form">Add a Manufacturer</NavLink>
                                </li>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Models
                            </a>
                            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li>
                                    <NavLink className="dropdown-item" to="/models">View Models</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/models/form">Add a Model</NavLink>
                                </li>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Automobiles
                            </a>
                            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li>
                                    <NavLink className="dropdown-item" to="/automobiles">View Automobiles</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/automobiles/form">Add an Automobile</NavLink>
                                </li>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Sales
                            </a>
                            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li>
                                    <NavLink className="dropdown-item" to="/salespeople">View Salespeople</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/salespeople/form">Add a Salesperson</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/salespeople/history">Salesperson History</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/customers">View Customers</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/customers/form">Add a Customer</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/sales">Sales</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/sales/form">Add a Sale</NavLink>
                                </li>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Service
                            </a>
                            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li>
                                    <NavLink className="dropdown-item" to="/add-technician">Add Technician</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/create-appointment">Create Appointment</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/service-appointments">Service Appointments</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/service-history">Service History</NavLink>
                                </li>
                                <li>
                                    <NavLink className="dropdown-item" to="/technicians">Technicians</NavLink>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};

export default Nav;
